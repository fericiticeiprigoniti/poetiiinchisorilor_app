import 'package:flutter/material.dart';
import 'package:poetii_inchisorilor/provider/font-size-notifier.dart';
import 'package:poetii_inchisorilor/screens/home-screen.dart';
import 'package:poetii_inchisorilor/screens/poem-screen.dart';
import 'package:poetii_inchisorilor/screens/poem-search-screen.dart';
import 'package:poetii_inchisorilor/screens/poet-screen.dart';
import 'package:poetii_inchisorilor/screens/poet-search-screen.dart';
import 'package:poetii_inchisorilor/screens/support-screen.dart';
import 'package:provider/provider.dart';
import 'dependency-configurator.dart';

void main() {
  configureDependencies();
  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider(create: (_) => FontSizeNotifier()),
    ],
    child: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Poetii inchisorilor',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        textTheme: TextTheme(
          headline1: TextStyle(
              fontFamily: 'Museo',
              fontSize: 30,
              color: Colors.black,
              fontWeight: FontWeight.bold),
          headline2: TextStyle(
              fontFamily: 'Museo',
              fontSize: 25,
              color: Colors.grey,
              fontWeight: FontWeight.bold),
        ),
      ),
      home: getIt<HomeScreen>(), //HomeScreen(),
      routes: {
        getIt<HomeScreen>().routeName: (context) => getIt<HomeScreen>(),
        //POETS
        getIt<PoetSearchScreen>().routeName: (context) =>
            getIt<PoetSearchScreen>(),
        //POEMS
        getIt<PoemSearchScreen>().routeName: (context) =>
            getIt<PoemSearchScreen>(),
        SupportScreen().routeName: (context) => SupportScreen(),
      },
      onGenerateRoute: (settings) {
        //POET SCREEN
        if (settings.name == PoetScreen.routeName) {
          final args = settings.arguments as PoetScreenArgs;

          return MaterialPageRoute(
            builder: (context) {
              return PoetScreen(args.uiService, args.personService, args.id);
            },
          );
        }
        //POEM SCREEN
        else if (settings.name == PoemScreen.routeName) {
          final args = settings.arguments as PoemScreenArgs;

          return MaterialPageRoute(
            builder: (context) {
              return PoemScreen(args.uiService, args.poemService, args.id);
            },
          );
        }
        assert(false, 'Need to implement ${settings.name}');
        return null;
      },
    );
  }
}
