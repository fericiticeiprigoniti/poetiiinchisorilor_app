class Endpoints {
  static final String apiAddress = "http://fcp.cpco.ro/api/";
  static final String endpointQuoteRandom = apiAddress + "citate/random";
  //PERSONS
  static final String endpointCommemorations = apiAddress + "personaje/comemorari";
  static final String endpointPoetSearchByLetter = apiAddress + "personaje/byletter/";
  static final String endpointPoetSearchByName = apiAddress + "personaje/list?searchTerm=";  
  static final String endpointPoetSearchById = apiAddress + "personaje/list?id=";
  
  //POEMS
  static final String endpointPoemList = apiAddress + "poezii/list";
  static final String endpointPoemSearchByTitle = apiAddress + "poezii/list?titleLike=";
  static final String endpointPoemGetById = apiAddress + "poezii/poezie/";  // + id
}
