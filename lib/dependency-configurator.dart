import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';

import 'dependency-configurator.config.dart';

final getIt = GetIt.instance;

@InjectableInit(
  initializerName: r'$initGetIt', // default
  preferRelativeImports: true, // default
  asExtension: false, // default
  // generateForDir: ['services']
)
void configureDependencies() => $initGetIt(getIt);