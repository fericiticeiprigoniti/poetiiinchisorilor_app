import 'package:flutter/material.dart';

class FontSizeNotifier with ChangeNotifier {
  double _fontSize = 20;

  double get fontSize => _fontSize;

  void increment() {
    _fontSize += 1;
    notifyListeners();
  }

  void decrement() {
    _fontSize -= 1;
    notifyListeners();
  }

  void reset() {
    _fontSize = 20;
    notifyListeners();
  }
}
