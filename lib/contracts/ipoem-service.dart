import 'package:poetii_inchisorilor/models/poem.dart';

abstract class IPoemService {
  Future<List<Poem>> getDailyPoems();
  Future<List<Poem>> getPoemsByTitle(String title);
  Future<Poem> getPoemById(int id);
}