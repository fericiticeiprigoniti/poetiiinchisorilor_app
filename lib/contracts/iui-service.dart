import 'package:flutter/material.dart';

abstract class IUiService {
  void showErrorMessage(BuildContext context, {String message = ""});
}