import 'package:poetii_inchisorilor/models/person-info.dart';

abstract class IPersonService {
  Future<List<PersonInfo>> getCommemorations();
  Future<List<PersonInfo>> searchByLetter(String letter);
  Future<List<PersonInfo>> searchByName(String name);
  Future<PersonInfo> searchById(int id);
}
