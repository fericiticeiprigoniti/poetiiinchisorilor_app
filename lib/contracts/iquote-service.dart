import 'package:poetii_inchisorilor/models/quote.dart';

abstract class IQuoteService {
  Future<Quote> getRandomQuote();
}
