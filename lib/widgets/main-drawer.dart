import 'package:flutter/material.dart';
import 'package:poetii_inchisorilor/dependency-configurator.dart';
import 'package:poetii_inchisorilor/screens/poem-search-screen.dart';
import 'package:poetii_inchisorilor/screens/poet-search-screen.dart';
import 'package:poetii_inchisorilor/screens/support-screen.dart';

class MainDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.only(
          topRight: Radius.circular(15.0), bottomRight: Radius.circular(15.0)),
      child: Drawer(
        child: SingleChildScrollView(
          child: Container(
            // decoration: BoxDecoration(
            //     gradient: LinearGradient(begin: Alignment.topLeft, stops: [
            //   0.5,
            //   0.9
            // ], colors: [
            //   Colors.white,
            //   Colors.red.withOpacity(.1)
            // ])),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 10.0, vertical: 20.0),
                  child: Center(
                    child: Image.asset(
                      "assets/images/logo_wide.png",
                      width: double.infinity,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width: double.infinity,
                        child: TextButton(
                          style: ButtonStyle(
                            alignment: Alignment.centerLeft,
                            padding: MaterialStateProperty.all(
                              EdgeInsets.all(0),
                            ),
                          ),
                          child: Text("Poetii",
                              textAlign: TextAlign.left,
                              style: Theme.of(context).textTheme.headline1),
                          onPressed: () => Navigator.pushNamed(
                              context, getIt<PoetSearchScreen>().routeName),
                        ),
                      ),
                      Container(
                        width: double.infinity,
                        child: TextButton(
                          style: ButtonStyle(
                            alignment: Alignment.centerLeft,
                            padding: MaterialStateProperty.all(
                              EdgeInsets.all(0),
                            ),
                          ),
                          child: Text("Poezii",
                              textAlign: TextAlign.left,
                              style: Theme.of(context).textTheme.headline1),
                          onPressed: () => Navigator.pushNamed(
                              context, getIt<PoemSearchScreen>().routeName),
                        ),
                      ),
                    ],
                  ),
                ),
                //MIDDLE IMAGE
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Image.asset(
                    'assets/images/temp_multiple.png',
                    width: double.infinity,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Despre proiect",
                          style: Theme.of(context).textTheme.headline2),
                      Text("Echipa",
                          style: Theme.of(context).textTheme.headline2),
                      TextButton(
                        style: ButtonStyle(
                          padding: MaterialStateProperty.all(
                            EdgeInsets.all(0),
                          ),
                        ),
                        child: Text("Sustine",
                            style: Theme.of(context).textTheme.headline2),
                        onPressed: () => {
                          Navigator.pushNamed(
                              context, SupportScreen().routeName)
                        },
                      ),
                      Text("Contact",
                          style: Theme.of(context).textTheme.headline2),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
