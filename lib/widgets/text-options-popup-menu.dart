import 'package:flutter/material.dart';
import 'package:poetii_inchisorilor/provider/font-size-notifier.dart';
import 'package:provider/provider.dart';

class TextOptionsPopupMenu extends StatelessWidget {
  void handleClick(BuildContext context, String value) {
    switch (value) {
      case OptionValues.increaseFont:
        context.read<FontSizeNotifier>().increment();
        break;
      case OptionValues.decreaseFont:
        context.read<FontSizeNotifier>().decrement();
        break;
      case OptionValues.resetDefaults:
        context.read<FontSizeNotifier>().reset();
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    const textStyle = TextStyle(fontFamily: 'Museo');
    const iconColor = Colors.red;

    return PopupMenuButton<String>(
      onSelected: (value) => handleClick(context, value),
      itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
        PopupMenuItem<String>(
          value: OptionValues.increaseFont,
          child: Row(
            children: [
              Icon(Icons.zoom_in, color: iconColor),
              SizedBox(width: 20),
              Text(OptionValues.increaseFont, style: textStyle),
            ],
          ),
        ),
        PopupMenuItem<String>(
          value: OptionValues.decreaseFont,
          child: Row(
            children: [
              Icon(Icons.zoom_out, color: iconColor),
              SizedBox(width: 20),
              Text(OptionValues.decreaseFont, style: textStyle),
            ],
          ),
        ),
        PopupMenuItem<String>(
          value: OptionValues.resetDefaults,
          child: Row(
            children: [
              Icon(Icons.text_fields, color: iconColor),
              SizedBox(width: 20),
              Text(OptionValues.resetDefaults, style: textStyle),
            ],
          ),
        ),
      ],
    );
  }
}

class OptionValues {
  static const increaseFont = "Mareste font";
  static const decreaseFont = "Micsoreaza font";
  static const resetDefaults = "Resetare valori";
}
