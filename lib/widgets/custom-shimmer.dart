import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class CustomShimmer extends StatelessWidget {
  final int rows;

  CustomShimmer(this.rows);

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      highlightColor: Colors.white,
      baseColor: Colors.grey[300] as Color,
      child: ShimmerLayout(this.rows),
      period: Duration(milliseconds: 800),
    );
  }
}

class ShimmerLayout extends StatelessWidget {
  final int rows;
  ShimmerLayout(this.rows) {
    _initChildren();
  }
  final double containerWidth = double.infinity;
  final double containerHeight = 15;

  final List<Widget> _childrenList = <Widget>[];
  void _initChildren() {
    for (var i = 0; i < rows; i++) {
      _childrenList.add(Container(
        height: containerHeight,
        width: containerWidth,
        color: Colors.grey,
      ));
      _childrenList.add(SizedBox(height: 5));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.symmetric(vertical: 7.5),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: _childrenList,
        ));
  }
}
