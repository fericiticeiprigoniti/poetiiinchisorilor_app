import 'package:flutter/material.dart';

class CustomSliverBar extends StatelessWidget {
  final String _imagePath;
  CustomSliverBar(this._imagePath);

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      elevation: 0,
      backgroundColor: Colors.white,
      expandedHeight: 200.0,
      floating: true,
      pinned: true,
      snap: true,
      leading: IconButton(
        icon: Icon(
          Icons.menu,
          color: Colors.black,
          size: 40,
        ),
        onPressed: () => Scaffold.of(context).openDrawer(),
      ),
      centerTitle: true,
      flexibleSpace: Stack(
        children: <Widget>[
          Positioned.fill(
              child: ClipRRect(
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(30),
                bottomRight: Radius.circular(30)),
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white
              ),
              child: Image.asset(
                this._imagePath,
                fit: BoxFit.contain,
              ),
            ),
          ))
        ],
      ),
    );
  }
}
