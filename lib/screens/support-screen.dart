import 'package:flutter/material.dart';

class SupportScreen extends StatelessWidget {
  final String routeName = '/support';

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Column(children: [
            Text(
                "Câteva moduri prin care ne poți susține sau prin care poți deveni parte din echipă:"),
            Text(
                "1.	Dacă ești literat, istoric, teolog, sau pur și simplu pasionat de acest subiect și bine pus la punct cu scrierea academică, atunci poți contribui cu studii, analize sau eseuri pe seama poeziei carcerale și a poeților detenției comuniste.Noi îți vom oferi o „tribună” pe această platformă."),
            Text(
                "2.	Dacă ești cercetător acreditat în cadrul C.N.S.A.S.,poți contribui cu studii, documente de arhivă sau ne poți indica dosare studiate care converg cu tema poeților și a poeziei de detenție."),
            Text(
                "3.	Dacă ești un pasionat al scrisului îngrijit, cu atenție la detalii, avem nevoie de tine pentrutranscrierea de materiale audio-video sau fotografiate. Știi să scrii cu diacritice și punctuație fără reproș? Atunci îți poți sufleca mânecile pentru transcrierea de documente inedite sau de înregistrări media, astfel încât aceste materiale să poată fi citite ușor de un număr cât mai mare de oameni.Ținta noastră nu este una neapărat „cantitativă”, dar urmărim să facem o treabă „ca la carte”."),
            Text(
                "4.	Dacă ești programator de meserie, și stăpânești bine unul sau mai multe dintre limbajele/frame-work-urile PHP, MySQL, React, HTML + CSS, jQuery, atunci îți urăm Welcome tothe club! Poate te întrebi ce mare lucru ar mai fi de făcut la acest proiect, ținând cont că există deja o echipă de programatori. Ei bine, explicația este că lucrăm la un proiect de anvergură națională din care poetiiinchisorilor.ro reprezintă doar o mică parte. În plus, fiecare programator lucrează pro-bono și rupe din timpul său de familie, timp care de cele mai multe ori este extrem de limitat. Prin urmare, orice linie de cod sau funcție în plus nu strică, ci din contră, constituie o cărămidă pusă laRețeaua Online a Memoriei Anticomuniste, rețea a cărei fundație este deja implementată. Rețeaua va reprezenta o suită de platforme educaționalecu o abordare multidisciplinară, respectiv culturală, istorică și teologică. Conținutul și prezentareavrem să le facem excepționale, din toate punctele de vedere. Și, fără modestie, considerăm că ceea ce am făcut până acum demonstrează că putem."),
            Text(
                "5.	Dacă ești urmaș de fost deținut politic, crede-ne pe cuvânt că arhiva de familie a înaintașului tău întemnițatconstituie o adevărată comoară din punct de vedere memorial. Câteva fotografii scanate, apoi trimise către noi pentru publicare online, și  câteva informații biografice esențiale, pe care nu le putem găsim nicăieri în altă parte, pot scoate din „anonimat” sau uitare o poveste de viață din care cu siguranță avem cu toții ceva de învățat."),
            Text(
                "6.	Susținere financiară, cu „bănuțul văduvei”.Vreme de câțiva ani am suportat costuri considerabile pentru constituirea unei arhive decente de cărți de memorialistică și istorie contemporană, dar mai ales de dosare digitalizate din arhiva fostei Securități. Zeci de mii de pagini! Aceasta este baza noastră documentară cu ajutorul căreia am reușit să dăm „viață” acestui proiect, șicare urmează să fie valorificată educațional în continuare. De asemenea, costurile de hosting pentru site le suportăm fără să transpirăm deocamdată. Dar ca să putem oferi în continuare informații inedite ne trebuie resurse financiarepentru obținerea de documentele digitalizate din dosarele aflate în arhiva C.N.S.A.S..Munca de documentare și sintetizare o facem noi, muncă care ia zeci de ore numai pentru a selecta dosarele valoroase din punct de vedere documentar, în vederea digitalizării. Însă digitalizarea costă, în prezent aplicându-se tariful de 0,2 lei pe pagină. Deși nu pare mult la prima vedere, atunci când te gândești că un singur dosar de 300 de file poate avea între 300-400 de pagini de interes, realizezi că costurile cresc considerabil. Și fiecare fost deținut politic are întocmite cel puțin 2, 3, 4 dosare pe numele său. Digitalizarea fotografiilor identificate în dosarele C.N.S.A.S. costă și ea la fel, iar când găsim fotografii cu siguranță nu ratăm nici una. Pe toate vrem să le scoatem din „anonimat”, astfel încât să redăm prezentului și posterității chipurile victimelor... dar și ale călăilor. Ceea ce documentăm însă nu se rezumă doar la poetiiinchisorilor.ro. Planul de bătaie e un pic mai mare și l-am sintetizat în secțiune destinată programatorilor."),
            Text(
                "7.	Un sfat bun nu strică niciodată, o critică constructivă la fel și cu atât mai mult nu strică o rugăciune."),
            Text("Ne gasesti la adresa ..."),
          ]),
        ),
      ),
    );
  }
}
