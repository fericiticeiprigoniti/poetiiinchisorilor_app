import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:poetii_inchisorilor/contracts/iperson-service.dart';
import 'package:poetii_inchisorilor/contracts/iui-service.dart';
import 'package:poetii_inchisorilor/models/person-info.dart';
import 'package:poetii_inchisorilor/widgets/main-drawer.dart';

@injectable
class PoetScreen extends StatefulWidget {
  static const String routeName = '/poet';

  final IUiService _uiService;
  final IPersonService _personService;
  final int _id;
  PoetScreen(this._uiService, this._personService, this._id);

  @override
  _PoetScreenState createState() => _PoetScreenState();
}

class _PoetScreenState extends State<PoetScreen> {
  PersonInfo? _personInfo;

  @override
  void initState() {
    super.initState();
    _searchById();
  }

  void _searchById() async {
    try {
      var response = await widget._personService.searchById(widget._id);
      setState(() {
        _personInfo = response;
      });
    } catch (e) {
      widget._uiService.showErrorMessage(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Image.asset(
            'assets/images/logo_wide.png',
            fit: BoxFit.contain,
          ),
          centerTitle: true,
          backgroundColor: Colors.white,
          foregroundColor: Colors.black,
          iconTheme: IconThemeData(color: Colors.black),
        ),
        drawer: MainDrawer(),
        body: _personInfo == null
            ? Center(child: CircularProgressIndicator())
            : SingleChildScrollView(
                child: Column(children: [
                  Image.network(
                    _personInfo?.avatarUrl ?? "",
                    width: 50,
                    height: 50,
                  ),
                ]),
              ),
      ),
    );
  }
}

class PoetScreenArgs {
  final IUiService uiService;
  final IPersonService personService;
  final int id;

  PoetScreenArgs(this.uiService, this.personService, this.id);
}
