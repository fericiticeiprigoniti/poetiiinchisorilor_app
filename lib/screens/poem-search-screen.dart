import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:poetii_inchisorilor/contracts/iui-service.dart';
import 'package:poetii_inchisorilor/contracts/ipoem-service.dart';
import 'package:poetii_inchisorilor/models/poem.dart';
import 'package:poetii_inchisorilor/screens/poem-screen.dart';
import 'package:poetii_inchisorilor/widgets/main-drawer.dart';
import 'package:flutter/services.dart';

@injectable
class PoemSearchScreen extends StatefulWidget {
  final String routeName = '/poem-search';
  final IUiService _uiService;
  final IPoemService _poemService;

  PoemSearchScreen(this._uiService, this._poemService);

  @override
  _PoemSearchScreenState createState() => _PoemSearchScreenState();
}

class _PoemSearchScreenState extends State<PoemSearchScreen> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _txtSearch = new TextEditingController();
  List<Poem> foundPoems = [];

  void _onSearchTap() async {
    try {
      if (!(_formKey.currentState?.validate() ?? true)) return;

      var response = await widget._poemService.getPoemsByTitle(_txtSearch.text);
      setState(() {
        foundPoems = response;
      });

      //hide keyboard
      SystemChannels.textInput.invokeMethod('TextInput.hide');
    } catch (e) {
      widget._uiService.showErrorMessage(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Image.asset(
            'assets/images/logo_wide.png',
            fit: BoxFit.contain,
          ),
          centerTitle: true,
          backgroundColor: Colors.white,
          foregroundColor: Colors.black,
          iconTheme: IconThemeData(color: Colors.black),
        ),
        drawer: MainDrawer(),
        body: Column(
          children: [
            //SEARCH BAR
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(children: [
                Form(
                  key: _formKey,
                  child: Expanded(
                    child: TextFormField(
                      controller: _txtSearch,
                      textInputAction: TextInputAction.search,
                      onFieldSubmitted: (value) => _onSearchTap(),
                      style: TextStyle(),
                      decoration: new InputDecoration(
                        filled: true,
                        fillColor: Colors.black12,
                        contentPadding: const EdgeInsets.only(
                            left: 14.0, bottom: 8.0, top: 8.0),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(25.7),
                        ),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(25.7),
                        ),
                        errorBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.red),
                          borderRadius: BorderRadius.circular(25.7),
                        ),
                        focusedErrorBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.red),
                          borderRadius: BorderRadius.circular(25.7),
                        ),
                        hintText: "Cauta o poezie",
                        hintStyle: TextStyle(fontFamily: 'Museo', fontSize: 20),
                      ),
                      validator: (value) {
                        if (value?.isEmpty ?? true) {
                          return "Introduceti un criteriu de cautare";
                        }
                        return null;
                      },
                    ),
                  ),
                ),
                ElevatedButton(
                  style: ButtonStyle(
                    padding: MaterialStateProperty.all(
                      EdgeInsets.all(0),
                    ),
                    backgroundColor: MaterialStateProperty.all(
                      Colors.red,
                    ),
                    minimumSize: MaterialStateProperty.all(
                      Size(45, 45),
                    ),
                  ),
                  child: Icon(Icons.search),
                  onPressed: () => _onSearchTap(),
                ),
              ]),
            ),
            //FOUND POEMS LIST VIEW
            Expanded(
              child: foundPoems.length == 0
                  ? Center(
                      child: Image.asset(
                        "assets/images/logo_tall.jpg",
                        width: MediaQuery.of(context).size.width * 0.7,
                        fit: BoxFit.contain,
                      ),
                    )
                  : ListView.builder(
                      itemCount: foundPoems.length,
                      itemBuilder: (context, i) {
                        var p = foundPoems[i];
                        return InkWell(
                          onTap: () => Navigator.pushNamed(
                              context, PoemScreen.routeName,
                              arguments: new PoemScreenArgs(widget._uiService,
                                  widget._poemService, p.id)),
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 10, vertical: 5),
                            child: Row(
                              children: [
                                Image.network(
                                  p.personajInfo.avatarUrl ?? "",
                                  width: 50,
                                  height: 50,
                                ),
                                Flexible(
                                  child: Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        '${p.personajInfo.nume} ${p.personajInfo.prenume}',
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                          fontFamily: 'Museo',
                                          fontSize: 18,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.red,
                                        ),
                                      ),
                                      Text(
                                        p.titlu,
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                          fontFamily: 'Museo',
                                          fontSize: 18,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black,
                                        ),
                                      ),
                                      Text(
                                        'Data: ${p.formattedDataPublicare}',
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                          fontFamily: 'Museo',
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.grey,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    ),
            ),
          ],
        ),
      ),
    );
  }
}
