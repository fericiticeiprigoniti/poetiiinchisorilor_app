import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:poetii_inchisorilor/contracts/iperson-service.dart';
import 'package:poetii_inchisorilor/contracts/ipoem-service.dart';
import 'package:poetii_inchisorilor/contracts/iquote-service.dart';
import 'package:poetii_inchisorilor/contracts/iui-service.dart';
import 'package:poetii_inchisorilor/models/person-info.dart';
import 'package:poetii_inchisorilor/models/poem.dart';
import 'package:poetii_inchisorilor/models/quote.dart';
import 'package:poetii_inchisorilor/screens/poem-screen.dart';
import 'package:poetii_inchisorilor/widgets/custom-shimmer.dart';
import 'package:poetii_inchisorilor/widgets/custom-sliver-bar.dart';
import 'package:poetii_inchisorilor/widgets/main-drawer.dart';

@injectable
class HomeScreen extends StatefulWidget {
  final String routeName = '/home';

  final IQuoteService _quoteService;
  final IPersonService _personService;
  final IPoemService _poemService;
  final IUiService _uiService;
  HomeScreen(this._quoteService, this._personService, this._poemService, this._uiService);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        drawer: MainDrawer(),
        body: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[CustomSliverBar('assets/images/logo.png')];
          },
          body: SingleChildScrollView(
            child: Column(
              children: [
                Stack(
                  children: [
                    // QUOTE SECTION
                    Positioned(
                      top: 10,
                      left: 10,
                      child: Image.asset(
                        'assets/images/quotes.png',
                        width: 40,
                        height: 40,
                        fit: BoxFit.contain,
                      ),
                    ),
                    Container(
                      width: double.infinity,
                      padding: const EdgeInsets.only(
                          top: 10, left: 55, right: 20, bottom: 10),
                      //RANDOM QUOTE
                      child: Center(
                        child: FutureBuilder(
                          future: widget._quoteService
                              .getRandomQuote(), // async work
                          builder: (BuildContext context,
                              AsyncSnapshot<Quote> snapshot) {
                            switch (snapshot.connectionState) {
                              case ConnectionState.waiting:
                                return CustomShimmer(5);
                              default:
                                if (snapshot.hasError)
                                  return Text(
                                    'A intervenit o eroare',
                                    style: TextStyle(
                                        fontFamily: 'Museo', fontSize: 20),
                                  );
                                else {
                                  var quote = snapshot.data;
                                  return Column(
                                    children: [
                                      Text(
                                        quote?.continut ?? "",
                                        style: TextStyle(
                                          fontFamily: 'Museo',
                                          fontStyle: FontStyle.italic,
                                          fontSize: 20,
                                          color: Colors.grey,
                                        ),
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: [
                                          Text(
                                            '${quote?.personajInfo == null ? "" : quote?.personajInfo.prenume ?? ""} ${quote?.personajInfo == null ? "" : quote?.personajInfo.nume}',
                                            style: TextStyle(
                                              fontFamily: 'Museo',
                                              fontSize: 20,
                                              color: Colors.red,
                                            ),
                                          ),
                                        ],
                                      )
                                    ],
                                  );
                                }
                            }
                          },
                        ),
                      ),
                    ),
                  ],
                ),
                // DAILY POEMS SECTION
                Container(
                  margin: const EdgeInsets.only(top: 30),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        flex: 1,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 10, right: 20.0),
                              child: Text(
                                'POEZIILE',
                                textAlign: TextAlign.right,
                                style: TextStyle(
                                  fontFamily: 'Museo',
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 10, right: 20.0),
                              child: Text(
                                'ZILEI',
                                textAlign: TextAlign.right,
                                style: TextStyle(
                                  fontFamily: 'Museo',
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Container(
                          //DAILY POEMS
                          child: FutureBuilder(
                            future: widget._poemService.getDailyPoems(),
                            builder: (BuildContext context,
                                AsyncSnapshot<List<Poem>> snapshot) {
                              switch (snapshot.connectionState) {
                                case ConnectionState.waiting:
                                  return CustomShimmer(5);
                                default:
                                  if (snapshot.hasError)
                                    return Text(
                                      'A intervenit o eroare',
                                      style: TextStyle(
                                          fontFamily: 'Museo', fontSize: 20),
                                    );
                                  else {
                                    return ListView(
                                      physics: NeverScrollableScrollPhysics(),
                                      shrinkWrap: true,
                                      children: snapshot.data == null
                                          ? []
                                          : snapshot.data
                                                  ?.map(
                                                    (p) => InkWell(
                                                      onTap: () => Navigator.pushNamed(
                                                          context,
                                                          PoemScreen.routeName,
                                                          arguments:
                                                              new PoemScreenArgs(
                                                                  widget
                                                                      ._uiService,
                                                                  widget
                                                                      ._poemService,
                                                                  p.id)),
                                                      child: Column(
                                                        children: [
                                                          Container(
                                                            child: Row(
                                                              children: [
                                                                Flexible(
                                                                  child: Column(
                                                                    mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .spaceAround,
                                                                    crossAxisAlignment:
                                                                        CrossAxisAlignment
                                                                            .start,
                                                                    children: [
                                                                      Text(
                                                                        '${p.personajInfo.nume} ${p.personajInfo.prenume}',
                                                                        textAlign:
                                                                            TextAlign.left,
                                                                        style:
                                                                            TextStyle(
                                                                          fontFamily:
                                                                              'Museo',
                                                                          fontSize:
                                                                              18,
                                                                          fontWeight:
                                                                              FontWeight.bold,
                                                                          color:
                                                                              Colors.red,
                                                                        ),
                                                                      ),
                                                                      Container(
                                                                        child:
                                                                            Text(
                                                                          p.titlu,
                                                                          style:
                                                                              TextStyle(
                                                                            fontFamily:
                                                                                'Museo',
                                                                            fontSize:
                                                                                18,
                                                                            fontWeight:
                                                                                FontWeight.bold,
                                                                            color:
                                                                                Colors.black,
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  )
                                                  .toList() ??
                                              [],
                                    );
                                  }
                              }
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                // COMMEMORATIONS
                Container(
                  margin: const EdgeInsets.only(top: 30),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        flex: 1,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 10, right: 20.0),
                              child: Text(
                                'COMEMORARI',
                                textAlign: TextAlign.right,
                                style: TextStyle(
                                  fontFamily: 'Museo',
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 10, right: 20.0),
                              child: Text(
                                'RECENTE',
                                textAlign: TextAlign.right,
                                style: TextStyle(
                                  fontFamily: 'Museo',
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Container(
                          //COMMEMORATIONS
                          child: FutureBuilder(
                            future: widget._personService.getCommemorations(),
                            builder: (BuildContext context,
                                AsyncSnapshot<List<PersonInfo>> snapshot) {
                              switch (snapshot.connectionState) {
                                case ConnectionState.waiting:
                                  return CustomShimmer(5);
                                default:
                                  if (snapshot.hasError)
                                    return Text(
                                      'A intervenit o eroare',
                                      style: TextStyle(
                                          fontFamily: 'Museo', fontSize: 20),
                                    );
                                  else {
                                    return ListView(
                                      physics: NeverScrollableScrollPhysics(),
                                      shrinkWrap: true,
                                      children: snapshot.data == null
                                          ? []
                                          : snapshot.data
                                                  ?.map(
                                                    (p) => Column(
                                                      children: [
                                                        Container(
                                                          padding:
                                                              const EdgeInsets
                                                                      .symmetric(
                                                                  horizontal: 0,
                                                                  vertical: 5),
                                                          child: Row(
                                                            children: [
                                                              Image.network(
                                                                p.avatarUrl ??
                                                                    "",
                                                                width: 50,
                                                                height: 50,
                                                              ),
                                                              Flexible(
                                                                child: Column(
                                                                  mainAxisAlignment:
                                                                      MainAxisAlignment
                                                                          .spaceAround,
                                                                  crossAxisAlignment:
                                                                      CrossAxisAlignment
                                                                          .start,
                                                                  children: [
                                                                    Text(
                                                                      '${p.nume} ${p.prenume}',
                                                                      textAlign:
                                                                          TextAlign
                                                                              .left,
                                                                      style:
                                                                          TextStyle(
                                                                        fontFamily:
                                                                            'Museo',
                                                                        fontSize:
                                                                            18,
                                                                        fontWeight:
                                                                            FontWeight.bold,
                                                                        color: Colors
                                                                            .red,
                                                                      ),
                                                                    ),
                                                                    Container(
                                                                      child:
                                                                          Text(
                                                                        p.formattedDataAdormire,
                                                                        style:
                                                                            TextStyle(
                                                                          fontFamily:
                                                                              'Museo',
                                                                          fontSize:
                                                                              15,
                                                                          color:
                                                                              Colors.grey,
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  )
                                                  .toList() ??
                                              [],
                                    );
                                  }
                              }
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
