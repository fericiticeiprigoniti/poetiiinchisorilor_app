import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:injectable/injectable.dart';
import 'package:poetii_inchisorilor/contracts/iperson-service.dart';
import 'package:poetii_inchisorilor/contracts/iui-service.dart';
import 'package:poetii_inchisorilor/models/person-info.dart';
import 'package:poetii_inchisorilor/screens/poet-screen.dart';
import 'package:poetii_inchisorilor/widgets/main-drawer.dart';

@injectable
class PoetSearchScreen extends StatefulWidget {
  final String routeName = '/poet-search';

  final IUiService _uiService;
  final IPersonService _personService;
  PoetSearchScreen(this._uiService, this._personService);

  @override
  _PoetSearchScreenState createState() => _PoetSearchScreenState();
}

class _PoetSearchScreenState extends State<PoetSearchScreen> {
  final _formKey = GlobalKey<FormState>();

  final TextEditingController _txtSearch = new TextEditingController();
  String? _selectedLetter;
  List<PersonInfo> foundPersons = [];

  void _searchPoetByLetter(String letter, BuildContext context) async {
    try {
      _resetData();

      var response = await widget._personService.searchByLetter(letter);
      setState(() {
        foundPersons = response;
      });

      //hide keyboard
      SystemChannels.textInput.invokeMethod('TextInput.hide');
    } catch (e) {
      widget._uiService.showErrorMessage(context);
    }
  }

  void _onSearchTap() async {
    try {
      _resetData();

      if (!(_formKey.currentState?.validate() ?? true)) return;

      var response = await widget._personService.searchByName(_txtSearch.text);
      setState(() {
        foundPersons = response;
      });

      //hide keyboard
      SystemChannels.textInput.invokeMethod('TextInput.hide');
    } catch (e) {
      widget._uiService.showErrorMessage(context);
    }
  }

  void _resetData() {
    setState(() {
      foundPersons = [];
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Image.asset(
            'assets/images/logo_wide.png',
            fit: BoxFit.contain,
          ),
          centerTitle: true,
          backgroundColor: Colors.white,
          foregroundColor: Colors.black,
          iconTheme: IconThemeData(color: Colors.black),
        ),
        drawer: MainDrawer(),
        body: Column(
          children: [
            //SEARCH BAR
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(children: [
                Form(
                  key: _formKey,
                  child: Expanded(
                    child: TextFormField(
                      controller: _txtSearch,
                      textInputAction: TextInputAction.search,
                      onFieldSubmitted: (value) => _onSearchTap(),
                      style: TextStyle(),
                      decoration: new InputDecoration(
                        filled: true,
                        fillColor: Colors.black12,
                        contentPadding: const EdgeInsets.only(
                            left: 14.0, bottom: 8.0, top: 8.0),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(25.7),
                        ),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(25.7),
                        ),
                        errorBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.red),
                          borderRadius: BorderRadius.circular(25.7),
                        ),
                        focusedErrorBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.red),
                          borderRadius: BorderRadius.circular(25.7),
                        ),
                        hintText: "Cauta un poet",
                        hintStyle: TextStyle(fontFamily: 'Museo', fontSize: 20),
                      ),
                      validator: (value) {
                        if (value?.isEmpty ?? true) {
                          return "Introduceti un criteriu de cautare";
                        }
                        return null;
                      },
                    ),
                  ),
                ),
                ElevatedButton(
                  style: ButtonStyle(
                    padding: MaterialStateProperty.all(
                      EdgeInsets.all(0),
                    ),
                    backgroundColor: MaterialStateProperty.all(
                      Colors.red,
                    ),
                    minimumSize: MaterialStateProperty.all(
                      Size(45, 45),
                    ),
                  ),
                  child: Icon(Icons.search),
                  onPressed: () => _onSearchTap(),
                ),
              ]),
            ),
            //LETTER SELECTION
            Container(
              width: double.infinity,
              padding: const EdgeInsets.symmetric(vertical: 15),
              child: Text(
                "LITERA ${_selectedLetter == null ? "" : _selectedLetter}",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontFamily: 'Museo',
                    fontSize: 25,
                    fontWeight: FontWeight.bold),
              ),
            ),
            Wrap(
              alignment: WrapAlignment.center,
              children: [
                AlphabetButton(
                    letter: 'A',
                    onPressed: () => _searchPoetByLetter('a', context)),
                AlphabetButton(
                    letter: 'B',
                    onPressed: () => _searchPoetByLetter('b', context)),
                AlphabetButton(
                    letter: 'C',
                    onPressed: () => _searchPoetByLetter('c', context)),
                AlphabetButton(
                    letter: 'D',
                    onPressed: () => _searchPoetByLetter('d', context)),
                AlphabetButton(
                    letter: 'E',
                    onPressed: () => _searchPoetByLetter('e', context)),
                AlphabetButton(
                    letter: 'F',
                    onPressed: () => _searchPoetByLetter('f', context)),
                AlphabetButton(
                    letter: 'G',
                    onPressed: () => _searchPoetByLetter('g', context)),
                AlphabetButton(
                    letter: 'H',
                    onPressed: () => _searchPoetByLetter('h', context)),
                AlphabetButton(
                    letter: 'I',
                    onPressed: () => _searchPoetByLetter('i', context)),
                AlphabetButton(
                    letter: 'J',
                    onPressed: () => _searchPoetByLetter('j', context)),
                AlphabetButton(
                    letter: 'K',
                    onPressed: () => _searchPoetByLetter('k', context)),
                AlphabetButton(
                    letter: 'L',
                    onPressed: () => _searchPoetByLetter('l', context)),
                AlphabetButton(
                    letter: 'M',
                    onPressed: () => _searchPoetByLetter('m', context)),
                AlphabetButton(
                    letter: 'N',
                    onPressed: () => _searchPoetByLetter('n', context)),
                AlphabetButton(
                    letter: 'O',
                    onPressed: () => _searchPoetByLetter('o', context)),
                AlphabetButton(
                    letter: 'P',
                    onPressed: () => _searchPoetByLetter('p', context)),
                AlphabetButton(
                    letter: 'Q',
                    onPressed: () => _searchPoetByLetter('q', context)),
                AlphabetButton(
                    letter: 'R',
                    onPressed: () => _searchPoetByLetter('r', context)),
                AlphabetButton(
                    letter: 'S',
                    onPressed: () => _searchPoetByLetter('s', context)),
                AlphabetButton(
                    letter: 'T',
                    onPressed: () => _searchPoetByLetter('t', context)),
                AlphabetButton(
                    letter: 'U',
                    onPressed: () => _searchPoetByLetter('u', context)),
                AlphabetButton(
                    letter: 'V',
                    onPressed: () => _searchPoetByLetter('v', context)),
                AlphabetButton(
                    letter: 'X',
                    onPressed: () => _searchPoetByLetter('x', context)),
                AlphabetButton(
                    letter: 'Z',
                    onPressed: () => _searchPoetByLetter('z', context)),
              ],
            ),
            //FOUND POEMS LIST VIEW
            Expanded(
              child: foundPersons.length == 0
                  ? Center(
                      child: Image.asset(
                        "assets/images/logo_tall.jpg",
                        width: MediaQuery.of(context).size.width * 0.7,
                        fit: BoxFit.contain,
                      ),
                    )
                  : ListView.builder(
                      itemCount: foundPersons.length,
                      itemBuilder: (context, i) {
                        var p = foundPersons[i];
                        return InkWell(
                          onTap: () => Navigator.pushNamed(
                              context, PoetScreen.routeName,
                              arguments: new PoetScreenArgs(widget._uiService,
                                  widget._personService, p.id)),
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 10, vertical: 5),
                            child: Row(
                              children: [
                                Image.network(
                                  p.avatarUrl ?? "",
                                  width: 50,
                                  height: 50,
                                ),
                                Flexible(
                                  child: Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        '${p.nume} ${p.prenume}',
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                          fontFamily: 'Museo',
                                          fontSize: 18,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.red,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    ),
            ),
          ],
        ),
      ),
    );
  }
}

class AlphabetButton extends StatelessWidget {
  const AlphabetButton(
      {Key? key, required this.letter, required this.onPressed})
      : super(key: key);

  final String letter;
  final Function() onPressed;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
        width: 25,
        height: 25,
        // color: Colors.red,
        // decoration: BoxDecoration(

        // ),
        child: Text(
          letter,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontFamily: 'Museo',
            fontSize: 20,
            color: Colors.grey,
          ),
        ),
      ),
      onTap: onPressed,
    );
  }
}
