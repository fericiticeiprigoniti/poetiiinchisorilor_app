import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:poetii_inchisorilor/contracts/ipoem-service.dart';
import 'package:poetii_inchisorilor/contracts/iui-service.dart';
import 'package:poetii_inchisorilor/models/poem.dart';
import 'package:poetii_inchisorilor/provider/font-size-notifier.dart';
import 'package:poetii_inchisorilor/widgets/main-drawer.dart';
import 'package:poetii_inchisorilor/widgets/text-options-popup-menu.dart';
import 'package:provider/provider.dart';

class PoemScreen extends StatefulWidget {
  static const String routeName = '/poem';

  final IUiService _uiService;
  final IPoemService _poemService;
  final int _id;
  PoemScreen(this._uiService, this._poemService, this._id);

  @override
  _PoemScreenState createState() => _PoemScreenState();
}

class _PoemScreenState extends State<PoemScreen> {
  Poem? _poem;

  @override
  void initState() {
    super.initState();
    _searchById();
  }

  void _searchById() async {
    try {
      var response = await widget._poemService.getPoemById(widget._id);
      setState(() {
        _poem = response;
      });
    } catch (e) {
      widget._uiService.showErrorMessage(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Image.asset(
            'assets/images/logo_wide.png',
            fit: BoxFit.contain,
          ),
          centerTitle: true,
          backgroundColor: Colors.white,
          foregroundColor: Colors.black,
          iconTheme: IconThemeData(color: Colors.black),
          actions: <Widget>[
            TextOptionsPopupMenu()
          ],
        ),
        drawer: MainDrawer(),
        body: _poem == null
            ? Center(child: CircularProgressIndicator())
            : SingleChildScrollView(
                child: Column(children: [
                  //TITLU
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 15.0, horizontal: 10),
                    child: Center(
                        child: SelectableText(
                      _poem?.titlu ?? "",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontFamily: 'Museo',
                          fontSize: context.watch<FontSizeNotifier>().fontSize + 5,
                          color: Colors.red,
                          fontWeight: FontWeight.bold),
                    )),
                  ),
                  //AUTOR
                  Container(
                    child: SelectableText(
                      "${_poem?.personajInfo.prenume ?? ""} ${_poem?.personajInfo.nume ?? ""}",
                      style: TextStyle(
                          fontFamily: 'Museo',
                          fontSize: context.watch<FontSizeNotifier>().fontSize,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  //CONTINUT
                  Container(
                    padding: EdgeInsets.all(15),
                    margin: EdgeInsets.only(bottom: 20),
                    child: Html(
                      data: _poem?.continut ?? "",
                      style: {
                        '*': Style(
                          fontFamily: 'Museo',
                          fontSize: FontSize(context.watch<FontSizeNotifier>().fontSize),
                        ),
                      },
                    ),
                  ),
                ]),
              ),
      ),
    );
  }
}

class PoemScreenArgs {
  final IUiService uiService;
  final IPoemService poemService;
  final int id;

  PoemScreenArgs(this.uiService, this.poemService, this.id);
}
