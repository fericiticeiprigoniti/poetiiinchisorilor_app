import 'dart:convert';
import 'package:injectable/injectable.dart';
import 'package:poetii_inchisorilor/contracts/iperson-service.dart';
import 'package:poetii_inchisorilor/models/person-info.dart';
import 'package:http/http.dart' as http;
import '../constants.dart';

@Injectable(as: IPersonService)
class PersonService implements IPersonService {
  @override
  Future<List<PersonInfo>> getCommemorations() async {
    var response = await http.get(Uri.parse(Endpoints.endpointCommemorations));
    final List<dynamic> parsedItems = json.decode(response.body)["data"];

    return parsedItems.map((e) => PersonInfo.fromJson(e)).toList();
  }

  @override
  Future<List<PersonInfo>> searchByLetter(String letter) async {
    var response = await http.get(Uri.parse(Endpoints.endpointPoetSearchByLetter + letter));
    final List<dynamic> parsedItems = json.decode(response.body)["data"];

    return parsedItems.map((e) => PersonInfo.fromJson(e)).toList();
  }

  @override
  Future<List<PersonInfo>> searchByName(String name) async {
    var response = await http.get(Uri.parse(Endpoints.endpointPoetSearchByName + name));
    final List<dynamic> parsedItems = json.decode(response.body)["data"];

    return parsedItems.map((e) => PersonInfo.fromJson(e)).toList();
  }

  @override
  Future<PersonInfo> searchById(int id) async {
    var response = await http.get(Uri.parse(Endpoints.endpointPoetSearchById + id.toString()));
    final List<dynamic> parsedItems = json.decode(response.body)["data"];

    return parsedItems.map((e) => PersonInfo.fromJson(e)).toList()[0];
  }
}
