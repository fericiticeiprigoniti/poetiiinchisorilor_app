import 'dart:convert';
import 'package:injectable/injectable.dart';
import 'package:poetii_inchisorilor/contracts/ipoem-service.dart';

import 'package:http/http.dart' as http;
import 'package:poetii_inchisorilor/models/poem.dart';
import '../constants.dart';

@Injectable(as: IPoemService)
class PoemService implements IPoemService {
  @override
  Future<List<Poem>> getDailyPoems() async {
    var response = await http.get(Uri.parse(Endpoints.endpointPoemList +
        "?perioadaCreatieiId=4&_orderBy=order_rand&_page-1&_per_page=2"));
    final List<dynamic> parsedItems = json.decode(response.body)["data"];

    return parsedItems.map((e) => Poem.fromJson(e)).toList();
  }

  @override
  Future<List<Poem>> getPoemsByTitle(String title) async {
    var response =
        await http.get(Uri.parse(Endpoints.endpointPoemSearchByTitle + title));
    final List<dynamic> parsedItems = json.decode(response.body)["data"];

    return parsedItems.map((e) => Poem.fromJson(e)).toList();
  }

  @override
  Future<Poem> getPoemById(int id) async {
    var response = await http
        .get(Uri.parse(Endpoints.endpointPoemGetById + id.toString()));
    final dynamic parsedItem = json.decode(response.body)[0];

    return Poem.fromJson(parsedItem);
  }
}
