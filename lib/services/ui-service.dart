import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:poetii_inchisorilor/contracts/iui-service.dart';

@Injectable(as: IUiService)
class UiService extends IUiService {
  void showErrorMessage(BuildContext context, {String message = ""}) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(
          message == "" ? "A intervenit o eroare" : message,
          style: TextStyle(color: Colors.white, fontFamily: 'Montserrat'),
        ),
        backgroundColor: Colors.red,
      ),
    );
  }
}
