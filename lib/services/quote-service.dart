import 'dart:convert';
import 'package:injectable/injectable.dart';
import 'package:poetii_inchisorilor/contracts/iquote-service.dart';
import 'package:poetii_inchisorilor/models/quote.dart';
import 'package:http/http.dart' as http;
import '../constants.dart';

@Injectable(as: IQuoteService)
class QuoteService implements IQuoteService {
  @override
  Future<Quote> getRandomQuote() async {
    var response = await http.get(Uri.parse(Endpoints.endpointQuoteRandom));
    final List<dynamic> parsedItems = json.decode(response.body)["data"];

    return Quote.fromJson(parsedItems[0]);
  }
}
