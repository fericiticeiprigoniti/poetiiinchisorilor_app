import 'package:intl/intl.dart';
import 'package:poetii_inchisorilor/models/person-info.dart';

class Poem {
  Poem(
      {required this.id,
      required this.titlu,
      required this.dataPublicare,
      required this.personajInfo,
      required this.continut});
  int id;
  String titlu;
  DateTime? dataPublicare;
  PersonInfo personajInfo;
  String continut;

  String get formattedDataPublicare {
    if (this.dataPublicare == null) return "";

    final DateFormat formatter = DateFormat('dd-MM-yyyy');
    return this.dataPublicare == null
        ? ""
        : formatter.format(this.dataPublicare as DateTime);
  }

  factory Poem.fromJson(Map<String, dynamic> parsedJson) {
    return Poem(
        id: int.parse(parsedJson['id'].toString()),
        titlu: parsedJson['titlu'].toString(),
        dataPublicare: parsedJson['dataPublicare'] == null
            ? null
            : DateTime.parse(parsedJson['dataPublicare'].toString()),
        personajInfo: PersonInfo.fromJson(parsedJson["personajInfo"]),
        continut: parsedJson['continut'].toString());
  }
}
