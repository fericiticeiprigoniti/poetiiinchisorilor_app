import 'package:intl/intl.dart';

class PersonInfo {
  PersonInfo(
      {required this.id, required this.nume, required this.prenume, this.dataAdormire, this.avatarUrl});
  int id;
  String nume;
  String prenume;
  DateTime? dataAdormire;
  String? avatarUrl;

  String get formattedDataAdormire {
    if (this.dataAdormire == null) return "";

    final DateFormat formatter = DateFormat('dd-MM-yyyy');
    return this.dataAdormire == null ? "" : formatter.format(this.dataAdormire as DateTime);
  }

  factory PersonInfo.fromJson(Map<String, dynamic> parsedJson) {
    return PersonInfo(
        id: int.parse(parsedJson['id'].toString()),
        nume: parsedJson['nume'].toString(),
        prenume: parsedJson['prenume'].toString(),
        dataAdormire: parsedJson['dataAdormire'] == null
            ? null
            : DateTime.parse(parsedJson['dataAdormire'].toString()),
        avatarUrl: parsedJson['avatar']?.toString());
  }
}
