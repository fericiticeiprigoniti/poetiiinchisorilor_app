import 'package:poetii_inchisorilor/models/person-info.dart';

class Quote {
  Quote({required this.id, required this.continut, required this.personajInfo});
  int id;
  String continut;
  PersonInfo personajInfo;

  factory Quote.fromJson(Map<String, dynamic> parsedJson) {
    return Quote(
        id: int.parse(parsedJson['id'].toString()),
        continut: parsedJson['continut'].toString(),
        personajInfo: PersonInfo.fromJson(parsedJson["personajInfo"]));
  }
}
