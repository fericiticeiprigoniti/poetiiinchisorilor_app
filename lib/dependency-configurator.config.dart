// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;

import 'contracts/iperson-service.dart' as _i3;
import 'contracts/ipoem-service.dart' as _i5;
import 'contracts/iquote-service.dart' as _i7;
import 'contracts/iui-service.dart' as _i9;
import 'screens/home-screen.dart' as _i14;
import 'screens/poem-search-screen.dart' as _i11;
import 'screens/poet-screen.dart' as _i12;
import 'screens/poet-search-screen.dart' as _i13;
import 'services/person-service.dart' as _i4;
import 'services/poem-service.dart' as _i6;
import 'services/quote-service.dart' as _i8;
import 'services/ui-service.dart'
    as _i10; // ignore_for_file: unnecessary_lambdas

// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
_i1.GetIt $initGetIt(_i1.GetIt get,
    {String? environment, _i2.EnvironmentFilter? environmentFilter}) {
  final gh = _i2.GetItHelper(get, environment, environmentFilter);
  gh.factory<_i3.IPersonService>(() => _i4.PersonService());
  gh.factory<_i5.IPoemService>(() => _i6.PoemService());
  gh.factory<_i7.IQuoteService>(() => _i8.QuoteService());
  gh.factory<_i9.IUiService>(() => _i10.UiService());
  gh.factory<_i11.PoemSearchScreen>(() =>
      _i11.PoemSearchScreen(get<_i9.IUiService>(), get<_i5.IPoemService>()));
  gh.factory<_i12.PoetScreen>(() => _i12.PoetScreen(
      get<_i9.IUiService>(), get<_i3.IPersonService>(), get<int>()));
  gh.factory<_i13.PoetSearchScreen>(() =>
      _i13.PoetSearchScreen(get<_i9.IUiService>(), get<_i3.IPersonService>()));
  gh.factory<_i14.HomeScreen>(() => _i14.HomeScreen(
      get<_i7.IQuoteService>(),
      get<_i3.IPersonService>(),
      get<_i5.IPoemService>(),
      get<_i9.IUiService>()));
  return get;
}
